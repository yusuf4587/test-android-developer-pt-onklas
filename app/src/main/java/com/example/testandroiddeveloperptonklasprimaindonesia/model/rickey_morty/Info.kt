package com.example.testandroiddeveloperptonklasprimaindonesia.model.rickey_morty

data class Info(
    val count: Int,
    val next: String,
    val pages: Int,
    val prev: String
)