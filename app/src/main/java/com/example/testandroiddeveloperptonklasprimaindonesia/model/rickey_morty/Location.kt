package com.example.testandroiddeveloperptonklasprimaindonesia.model.rickey_morty

data class Location(
    val name: String,
    val url: String
)