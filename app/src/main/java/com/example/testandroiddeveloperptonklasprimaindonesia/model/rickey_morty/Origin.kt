package com.example.testandroiddeveloperptonklasprimaindonesia.model.rickey_morty

data class Origin(
    val name: String,
    val url: String
)