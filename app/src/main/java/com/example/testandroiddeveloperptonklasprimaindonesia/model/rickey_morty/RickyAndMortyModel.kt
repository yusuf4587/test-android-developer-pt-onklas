package com.example.testandroiddeveloperptonklasprimaindonesia.model.rickey_morty

data class RickyAndMortyModel(
    val info: Info,
    val results: List<Result>
)