package com.example.testandroiddeveloperptonklasprimaindonesia.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "MakananMinuman")

@Parcelize
data class MakananDanMinumanModel (
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")var id :Int= 0,
    @ColumnInfo(name = "nama")var nama :String= "",
    @ColumnInfo(name = "foto")var foto :String= "",
    @ColumnInfo(name = "harga")var harga :Double= 0.0,
) : Parcelable {
}
