package com.example.testandroiddeveloperptonklasprimaindonesia.makanan_minuman

import android.R.attr
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.MenuInflater
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage
import com.example.testandroiddeveloperptonklasprimaindonesia.R
import com.example.testandroiddeveloperptonklasprimaindonesia.db.MakanaMinumanRoomDb
import com.example.testandroiddeveloperptonklasprimaindonesia.db.MakananMinumanDao
import com.example.testandroiddeveloperptonklasprimaindonesia.model.MakananDanMinumanModel
import kotlinx.android.synthetic.main.activity_makanan_minuman_edit.*
import kotlinx.android.synthetic.main.list_item.view.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.lang.Exception
import java.net.URI
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*


class MakananMinumanEdit : AppCompatActivity() {

    private val TAG = "MakananMinumanEdit"

    private val PICK_IMAGE_REQUEST = 1111 //request code

    val EDIT_EXTRA = "edit_makanan_minuman_extra"
    private lateinit var makanan: MakananDanMinumanModel
    private var isUpdate = false
    private lateinit var database: MakanaMinumanRoomDb
    private lateinit var dao: MakananMinumanDao

    private var img_base64 = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_makanan_minuman_edit)

        database = MakanaMinumanRoomDb.getDatabase(applicationContext)
        dao = database.getMakananMinumanDao()

        if (intent.getParcelableExtra<MakananDanMinumanModel>(EDIT_EXTRA) != null) {
            btn_delete.visibility = View.VISIBLE
            isUpdate = true
            makanan = intent.getParcelableExtra(EDIT_EXTRA)!!

            edt_nama.setText(makanan.nama)


            var originalString = makanan.harga.toString().split(".")
            val longval: Long

            longval = originalString[0].toLong()
            val formatter = NumberFormat.getInstance(Locale.US) as DecimalFormat
            formatter.applyPattern("#,###,###,###,###")
            val formattedString = formatter.format(longval)

            //setting text after format to EditText
            edt_harga.setText(formattedString)
            edt_harga.setSelection(edt_harga.getText().length)



            imageView.setImageBitmap(base64ToBitmap(makanan.foto))

            img_base64 = makanan.foto

        }else{
            btn_delete.visibility = View.GONE
        }

        imageView.setOnClickListener {

            val intent = Intent()
                .setType("image/*")
                .setAction(Intent.ACTION_GET_CONTENT)

            startActivityForResult(
                Intent.createChooser(intent, "Select a file"),
                PICK_IMAGE_REQUEST
            )
        }

        btn_simpan.setOnClickListener {

            if (fieldCheck()) {
                val nama = edt_nama.text.toString()
                var harga = 0.0
                val foto = img_base64

                Log.d(TAG, "onCreate: harga ${edt_harga.text.toString()}", )

                var originalString = edt_harga.text.toString()
                if (originalString.contains(",")) {
                    originalString = originalString.replace(",".toRegex(), "")
                    harga = originalString.toDouble()
                    Log.d(TAG, "onCreate: $harga 1111111", )
                }else{
                    harga=originalString.toDouble()
                    Log.d(TAG, "onCreate: $harga 222222222", )
                }

                if (isUpdate) {
                    saveMakanan(
                        MakananDanMinumanModel(
                            id = makanan.id,
                            nama = nama,
                            foto = foto,
                            harga = harga,
                        )
                    )

                } else {
                    saveMakanan(
                        MakananDanMinumanModel(
                            nama = nama,
                            foto = foto,
                            harga = harga,
                        )
                    )
                }
                finish()
            }
        }

        btn_delete.setOnClickListener {
            deleteData(makanan)

        }

        edt_harga.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                edt_harga.removeTextChangedListener(this)
                try {
                    var originalString = s.toString()
                    val longval: Long
                    if (originalString.contains(",")) {
                        originalString = originalString.replace(",".toRegex(), "")
                    }
                    longval = originalString.toLong()
                    val formatter = NumberFormat.getInstance(Locale.US) as DecimalFormat
                    formatter.applyPattern("#,###,###,###,###")
                    val formattedString = formatter.format(longval)

                    //setting text after format to EditText
                    edt_harga.setText(formattedString)
                    edt_harga.setSelection(edt_harga.getText().length)
                } catch (nfe: NumberFormatException) {
                    nfe.printStackTrace()
                }
                edt_harga.addTextChangedListener(this)
            }
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        })

    }

    private fun fieldCheck(): Boolean {
        var err = 0
        if (edt_nama.text.isEmpty()) {
            text_input_nama.error = getString(R.string.emp_nama)
            err++
        } else {
            text_input_nama.error = ""
        }

        if (edt_harga.text.isEmpty()) {
            text_input_harga.error = getString(R.string.emp_harga)
            err++
        } else {
            text_input_harga.error = ""
        }


        if (img_base64 == "") {
            Toast.makeText(
                applicationContext,
                resources.getString(R.string.emp_img),
                Toast.LENGTH_SHORT
            ).show()
            err++
        }


        return err <= 0
    }

    private fun saveMakanan(makanan: MakananDanMinumanModel) {

        if (dao.getById(makanan.id).isEmpty()) {

            dao.insert(makanan)
        } else {

            dao.update(makanan)
        }

        Toast.makeText(applicationContext, "Note saved", Toast.LENGTH_SHORT).show()
    }


    @RequiresApi(Build.VERSION_CODES.N)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === PICK_IMAGE_REQUEST) {
            if (resultCode === RESULT_OK) {

                if (data != null) {
                    data.data?.let { imgBuild(data = it) }
                }


            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun imgBuild(data: Uri) {
        val filePath = data
        val imageName = filePath?.let { getFileName(it) }.toString()
        val fileType = imageName.split(".")
        val lastIndex = fileType.lastIndex
        val extension = fileType[lastIndex].toLowerCase()

            try {
                val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, filePath)

                val imputStream = this.getContentResolver().openInputStream(filePath!!)

                val ei: ExifInterface? = ExifInterface(imputStream!!)
                val orientation = ei?.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED
                )
                var rotatedBitmap: Bitmap? = null
                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap =
                        rotateImage(bitmap, 90)
                    ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap =
                        rotateImage(bitmap, 180)
                    ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap =
                        rotateImage(bitmap, 270)
                    ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = bitmap
                    else -> rotatedBitmap = bitmap
                }


                val resized = rotatedBitmap?.let {
                    Bitmap.createScaledBitmap(it, (rotatedBitmap.width*0.5).toInt(),
                        (rotatedBitmap.height*0.5).toInt(), true)
                }
                img_base64 = resized?.let { getStringImage(it) }.toString()
                imageView.setImageBitmap(resized)
            }catch (e:Exception){
            }

    }

    fun getFileName(uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = this.contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                cursor!!.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }


    fun getStringImage(bmp: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val imageBytes = baos.toByteArray()
        return Base64.encodeToString(imageBytes, Base64.DEFAULT)
    }

    fun base64ToBitmap(base64String: String):Bitmap {
        val imageBytes = Base64.decode(base64String, Base64.DEFAULT)
        val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        return  decodedImage
    }

    private fun deleteData(makanan: MakananDanMinumanModel) {
        val dialog = AlertDialog.Builder(this, R.style.ThemeOverlay_AppCompat_Dialog)
        dialog.setTitle(getString(R.string.txt_clear_note))
            .setMessage(getString(R.string.txt_clear_note_sure))
            .setPositiveButton(android.R.string.ok) { _, _ ->
                dao.delete(makanan)
                Toast.makeText(applicationContext, resources.getString(R.string.txt_berhasil_dihapus), Toast.LENGTH_SHORT).show()
                finish()
            }.setNegativeButton(android.R.string.cancel, null).create().show()
    }





}

