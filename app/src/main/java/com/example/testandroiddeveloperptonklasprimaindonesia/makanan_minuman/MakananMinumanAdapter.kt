package com.example.testandroiddeveloperptonklasprimaindonesia.makanan_minuman

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.resource.bitmap.TransformationUtils
import com.example.testandroiddeveloperptonklasprimaindonesia.GlobalFunction
import com.example.testandroiddeveloperptonklasprimaindonesia.R
import com.example.testandroiddeveloperptonklasprimaindonesia.model.MakananDanMinumanModel
import kotlinx.android.synthetic.main.activity_makanan_minuman_edit.*
import kotlinx.android.synthetic.main.list_item.view.*
import java.util.*

class MakananMinumanAdapter(
    private val listItems: ArrayList<MakananDanMinumanModel>,
    private val listener: MakananMinumanInterface
) :RecyclerView.Adapter<MakananMinumanAdapter.AdapterViewHolder>(){

    companion object{
       private val TAG = "MakananMinumanAdapter"
    }

    inner class AdapterViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        @RequiresApi(Build.VERSION_CODES.N)
        fun bind(postResponse: MakananDanMinumanModel){
            with(itemView){
                itemView.nama.text = postResponse.nama
                itemView.harga.text = GlobalFunction.rupiah(postResponse.harga)


                var convertStringToUri = Uri.parse(postResponse.foto)
                Log.d(TAG, "bind:convertStringToUri  ${convertStringToUri}")
                Log.d(TAG, "bind:postResponse.foto  ${postResponse.foto}")

                itemView.img.setImageBitmap(base64ToBitmap(postResponse.foto))


                card_view_item.setOnClickListener {
                    listener.OnItemClicked(postResponse)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item,
            parent,
            false
        )
        return AdapterViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {
        holder.bind(listItems[position])

    }

    override fun getItemCount(): Int = listItems.size


    fun base64ToBitmap(base64String: String):Bitmap {
        val imageBytes = Base64.decode(base64String, Base64.DEFAULT)
        val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        return  decodedImage
    }


}
