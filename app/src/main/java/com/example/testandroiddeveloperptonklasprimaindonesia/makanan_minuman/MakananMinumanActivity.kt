package com.example.testandroiddeveloperptonklasprimaindonesia.makanan_minuman

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.OrientationHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.testandroiddeveloperptonklasprimaindonesia.R
import com.example.testandroiddeveloperptonklasprimaindonesia.db.MakanaMinumanRoomDb
import com.example.testandroiddeveloperptonklasprimaindonesia.model.MakananDanMinumanModel
import kotlinx.android.synthetic.main.activity_makanan_minuman_activity.*
import kotlinx.android.synthetic.main.activity_makanan_minuman_edit.*


class MakananMinumanActivity : AppCompatActivity(),MakananMinumanInterface {

    lateinit var listItems :ArrayList<MakananDanMinumanModel>
    lateinit var makananMinumanAdapter: MakananMinumanAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_makanan_minuman_activity)
        swipe_container.isRefreshing = true

        getAllData()

        floatingActionButton.setOnClickListener {
            startActivity(Intent(this, MakananMinumanEdit::class.java))
        }

        swipe_container.setOnRefreshListener {
            getAllData()
        }

        filter.setOnClickListener {
            menuFilter(
                filter,
                this
            )
        }
        txt_Search.doAfterTextChanged {
            if(txt_Search.length()>0){
                Handler().postDelayed({
                    search(txt_Search.text.toString())
                },200)
            }else{
                Handler().postDelayed({
                    getAllData()
                },200)
            }
        }

    }

    private fun getAllData(){
        val database = MakanaMinumanRoomDb.getDatabase(applicationContext)
        val dao = database.getMakananMinumanDao()
        listItems = ArrayList<MakananDanMinumanModel>()
        listItems.clear()
        listItems.addAll(dao.getAll())
        setupRecyclerview(recycler_view_main)

        swipe_container.isRefreshing = false
        if(listItems.size>0){
            text_view_note_empty.visibility = View.GONE
        }else{
            text_view_note_empty.visibility = View.VISIBLE
        }
    }

    private fun setupRecyclerview(recyclerView: RecyclerView) {
        try {
            recyclerView.setHasFixedSize(true)
            val manager = StaggeredGridLayoutManager(2, OrientationHelper.VERTICAL)
            manager.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS;
            manager.isAutoMeasureEnabled = true
            recyclerView.layoutManager = manager
            makananMinumanAdapter = MakananMinumanAdapter(listItems, this)
            recyclerView.adapter = makananMinumanAdapter
        }catch (e: Exception){ }
    }

    override fun OnItemClicked(makananDanMinumanModel: MakananDanMinumanModel) {
        val intent = Intent(this, MakananMinumanEdit::class.java)
        intent.putExtra(MakananMinumanEdit().EDIT_EXTRA, makananDanMinumanModel)
        startActivity(intent)
    }


    override fun onResume() {
        super.onResume()
        getAllData()

    }


    private fun menuFilter(v: View, context: Context) {
        val popup = PopupMenu(context, v)
        val inflater: MenuInflater = popup.menuInflater
        inflater.inflate(R.menu.menu, popup.menu)
        popup.show()

        popup.setOnMenuItemClickListener(object : MenuItem.OnMenuItemClickListener,
            PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {
                if(item.title == "Ascending"){
                    getAllDataAsc()
                }else if(item.title == "Descending"){
                    getAllDataDesc()
                }else{
                    getAllData()
                }

                return true
            }
        })
    }
    private fun getAllDataAsc(){
        val database = MakanaMinumanRoomDb.getDatabase(applicationContext)
        val dao = database.getMakananMinumanDao()
        listItems = ArrayList<MakananDanMinumanModel>()
        listItems.clear()
        listItems.addAll(dao.getAllOrderByAsc())
        setupRecyclerview(recycler_view_main)

        swipe_container.isRefreshing = false
        if(listItems.size>0){
            text_view_note_empty.visibility = View.GONE
        }else{
            text_view_note_empty.visibility = View.VISIBLE
        }
    }

    private fun getAllDataDesc(){
        val database = MakanaMinumanRoomDb.getDatabase(applicationContext)
        val dao = database.getMakananMinumanDao()
        listItems = ArrayList<MakananDanMinumanModel>()
        listItems.clear()
        listItems.addAll(dao.getAllOrderByDesc())
        setupRecyclerview(recycler_view_main)

        swipe_container.isRefreshing = false
        if(listItems.size>0){
            text_view_note_empty.visibility = View.GONE
        }else{
            text_view_note_empty.visibility = View.VISIBLE
        }
    }


    private fun search(key:String){
        val database = MakanaMinumanRoomDb.getDatabase(applicationContext)
        val dao = database.getMakananMinumanDao()
        listItems = ArrayList<MakananDanMinumanModel>()
        listItems.clear()
        listItems.addAll(dao.filterName("%$key%"))
        setupRecyclerview(recycler_view_main)

        swipe_container.isRefreshing = false
        if(listItems.size>0){
            text_view_note_empty.visibility = View.GONE
        }else{
            text_view_note_empty.visibility = View.VISIBLE
        }
    }



}