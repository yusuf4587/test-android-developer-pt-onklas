package com.example.testandroiddeveloperptonklasprimaindonesia

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.testandroiddeveloperptonklasprimaindonesia.makanan_minuman.MakananMinumanActivity
import com.example.testandroiddeveloperptonklasprimaindonesia.ricky_morty.RickyAndMortyActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn1.setOnClickListener {
            startActivity(Intent(this,MakananMinumanActivity::class.java))
        }
        btn2.setOnClickListener {
            startActivity(Intent(this,RickyAndMortyActivity::class.java))

        }

    }
}