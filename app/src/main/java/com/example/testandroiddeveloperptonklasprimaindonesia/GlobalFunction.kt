package com.example.testandroiddeveloperptonklasprimaindonesia

import java.text.NumberFormat
import java.util.*

class GlobalFunction {
    companion object{
        fun rupiah(number: Double): String{
            val localeID =  Locale("in", "ID")
            val numberFormat = NumberFormat.getCurrencyInstance(localeID)
            val split =  numberFormat.format(number).toString().split(",")[0]
            return split
        }
    }


}