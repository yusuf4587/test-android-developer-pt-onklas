package com.example.testandroiddeveloperptonklasprimaindonesia.db

import androidx.room.*
import com.example.testandroiddeveloperptonklasprimaindonesia.model.MakananDanMinumanModel

@Dao
interface MakananMinumanDao {

    @Insert
    fun insert(makananMinuman:MakananDanMinumanModel)

    @Update
    fun update(makananMinuman:MakananDanMinumanModel)

    @Delete
    fun delete(makananMinuman:MakananDanMinumanModel)

    @Query("SELECT * FROM MakananMinuman")
    fun getAll() : List<MakananDanMinumanModel>

    @Query("SELECT * FROM MakananMinuman ORDER BY nama ASC")
    fun getAllOrderByAsc() : List<MakananDanMinumanModel>

    @Query("SELECT * FROM MakananMinuman ORDER BY nama DESC")
    fun getAllOrderByDesc() : List<MakananDanMinumanModel>



    @Query("SELECT * FROM MakananMinuman where id = :id")
    fun getById(id:Int):List<MakananDanMinumanModel>

    @Query("SELECT * FROM MakananMinuman where nama LIKE :nama ")
    fun filterName(nama:String):List<MakananDanMinumanModel>



}