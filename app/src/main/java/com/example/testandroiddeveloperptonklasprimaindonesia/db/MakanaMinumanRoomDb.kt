package com.example.testandroiddeveloperptonklasprimaindonesia.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.testandroiddeveloperptonklasprimaindonesia.model.MakananDanMinumanModel

@Database(entities = [MakananDanMinumanModel::class],version = 1,exportSchema = false)
abstract class MakanaMinumanRoomDb: RoomDatabase() {

    companion object{
        @Volatile
        private var INSTANCE:MakanaMinumanRoomDb? = null

        fun getDatabase(context: Context): MakanaMinumanRoomDb{
            return INSTANCE ?: synchronized(this){
                // create database
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MakanaMinumanRoomDb::class.java,
                    "MakananMinuman_db"
                )
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build()

                INSTANCE = instance
                instance

            }
        }
    }

    abstract fun getMakananMinumanDao():MakananMinumanDao
}