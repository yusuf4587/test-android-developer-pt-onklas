package com.example.testandroiddeveloperptonklasprimaindonesia.ricky_morty

import android.annotation.SuppressLint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.OrientationHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.testandroiddeveloperptonklasprimaindonesia.R
import com.example.testandroiddeveloperptonklasprimaindonesia.model.rickey_morty.Result
import com.example.testandroiddeveloperptonklasprimaindonesia.model.rickey_morty.RickyAndMortyModel
import com.example.testandroiddeveloperptonklasprimaindonesia.ricky_morty.api.Client
import kotlinx.android.synthetic.main.activity_ricky_and_morty_activity.*
import retrofit2.Call
import retrofit2.Response


class RickyAndMortyActivity : AppCompatActivity() {
    private val first = "page=1"
    private var loadNext = ""

    private lateinit var listItems: ArrayList<Result>
    private lateinit var mlistingAdapter : RickeyMortyAdapter

    private var loadMore = false    // First load

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ricky_and_morty_activity)
        swipe_container.isRefreshing = true
        rvListener()
        rvList(recyclerview)
        loadRickymorty(first)

        swipe_container.setOnRefreshListener {
            listItems.clear()
            mlistingAdapter.notifyDataSetChanged()
            loadNext=""
            loadRickymorty(first)
        }


    }
    private fun loadRickymorty(page:String) {
        if(recyclerview!=null){
            Client.instance.getcharacter("character?$page").enqueue(object :
                retrofit2.Callback<RickyAndMortyModel> {
                @RequiresApi(Build.VERSION_CODES.KITKAT)
                override fun onResponse(
                    call: Call<RickyAndMortyModel>,
                    response: Response<RickyAndMortyModel>
                ) {
                    loadMore = false
                    if (swipe_container != null) {
                        swipe_container.isRefreshing = false
                    }
                    try{
                        progressBar.visibility = View.INVISIBLE

                        if(response.body()!!.info.next != "null"){
                            val split_url = response.body()!!.info.next.split("?")
                            loadNext = split_url[1]
                        }
                        listItems.addAll(response.body()!!.results)
                        mlistingAdapter.notifyItemInserted(listItems.size)

                    }catch (e:Exception){

                    }
                }
                override fun onFailure(call: Call<RickyAndMortyModel>, t: Throwable) {
                    try {
                        loadMore = false
                        progressBar.visibility = View.INVISIBLE

                        Log.d("uch", "response Throwable loadRickymorty -- $t")
                    }catch (e:java.lang.Exception){}

                }
            })
        }
    }

    private fun rvList(recyclerView: RecyclerView) {
        try {
            listItems = ArrayList<Result>()
            val manager = StaggeredGridLayoutManager(2, OrientationHelper.VERTICAL)
            manager.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS;
            manager.isAutoMeasureEnabled = true
            recyclerView.layoutManager = manager
            mlistingAdapter = RickeyMortyAdapter(listItems)
            recyclerView.adapter = mlistingAdapter
        }catch (e:Exception){ }
    }

    @SuppressLint("ResourceAsColor")
    private fun rvListener() {

        try {
            recyclerview.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy > 0) { //check for scroll down
                        val linearLayoutManager =
                            recyclerView?.layoutManager as StaggeredGridLayoutManager
                        var visibleItemCount = linearLayoutManager.getChildCount()
                        var totalItemCount = linearLayoutManager.getItemCount()

                        val position: Int =
                            linearLayoutManager.findFirstVisibleItemPositions(null).get(
                                0
                            )

                        if (!loadMore) {
                            if (visibleItemCount + position >= totalItemCount) {

                                loadMore = true

                                if (progressBar != null) {
                                    progressBar.visibility = View.VISIBLE
                                }
                                Handler().run {
                                    loadRickymorty(loadNext)
                                }
                            }
                        }
                    }
                }
            })
        }catch (E: Exception){}
    }

}