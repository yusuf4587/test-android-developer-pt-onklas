package com.example.testandroiddeveloperptonklasprimaindonesia.ricky_morty

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.testandroiddeveloperptonklasprimaindonesia.GlobalFunction
import com.example.testandroiddeveloperptonklasprimaindonesia.R
import com.example.testandroiddeveloperptonklasprimaindonesia.makanan_minuman.MakananMinumanAdapter
import com.example.testandroiddeveloperptonklasprimaindonesia.makanan_minuman.MakananMinumanInterface
import com.example.testandroiddeveloperptonklasprimaindonesia.model.MakananDanMinumanModel
import com.example.testandroiddeveloperptonklasprimaindonesia.model.rickey_morty.Result
import com.example.testandroiddeveloperptonklasprimaindonesia.model.rickey_morty.RickyAndMortyModel
import kotlinx.android.synthetic.main.list_item.view.*
import java.util.ArrayList

class RickeyMortyAdapter (
    private val listItems: ArrayList<Result>,
) : RecyclerView.Adapter<RickeyMortyAdapter.AdapterViewHolder>(){

    companion object{
        private val TAG = "MakananMinumanAdapter"
    }

    inner class AdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        @RequiresApi(Build.VERSION_CODES.N)
        fun bind(postResponse: Result){
            with(itemView){

                Glide.with(context).load(postResponse.image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform().into(itemView.img)

                itemView.nama.text = postResponse.name
                itemView.harga.text = postResponse.gender

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item,
            parent,
            false
        )
        return AdapterViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {
        holder.bind(listItems[position])

    }

    override fun getItemCount(): Int = listItems.size




}
